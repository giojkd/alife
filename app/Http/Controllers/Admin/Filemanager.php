<?php

namespace App\Http\Controllers\Admin;

use App\EmailNotificationSetting;
use App\Helper\Reply;
use App\Http\Requests\SmtpSetting\UpdateSmtpSetting;
use App\Notifications\TestEmail;
use App\SmtpSetting;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Filemanager extends AdminBaseController
{

    public function index() {

        $this->data['pageIcon'] = '';
        $this->data['pageTitle'] = '';
        return view('admin.filemanager.index', $this->data);
    }




}
