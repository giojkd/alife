<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $dates = ['purchase_date'];

	protected $fillable = ['item_name','purchase_date','purchase_from','price','currency_id','task_id','expense_type_id','user_id'];

    public function currency(){
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id')->withoutGlobalScopes(['active']);
    }
}
