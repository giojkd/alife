@extends('layouts.app')
@section('page-title')
<div class="row bg-title">
	<!-- .page title -->
	<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
		<h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
	</div>
	<!-- /.page title -->
	<!-- .breadcrumb -->
	<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
		<ol class="breadcrumb">
			<li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
			<li><a href="{{ route('admin.all-tasks.index') }}">{{ $pageTitle }}</a></li>
			<li class="active">@lang('app.edit')</li>
		</ol>
	</div>
	<!-- /.breadcrumb -->
</div>
@endsection
@push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">
<style>
.panel-black .panel-heading a, .panel-inverse .panel-heading a {
	color: unset!important;
}
</style>
@endpush
@section('content')


<div class="row">
	<div class="col-md-12">

		<div class="content-wrap">
			<section id="section-line-3" class="show">
				<div class="row">
					<div class="col-md-12" id="files-list-panel">
						<div class="white-box">
							<h2>@lang('modules.projects.files')</h2>

							<div class="row m-b-10">
								<div class="col-md-12">
									<a href="javascript:;" id="show-dropzone"
									class="btn btn-success btn-outline"><i class="ti-upload"></i> @lang('modules.projects.uploadFile')</a>
								</div>
							</div>

							<div class="row m-b-20 hide" id="file-dropzone">
								<div class="col-md-12">
									<form action="{{ route('admin.files.store') }}" class="dropzone"id="file-upload-dropzone">
										{!! Form::hidden('project_id', $task->project_id) !!}
										{!! Form::hidden('task_id',$task->id) !!}
										{!! Form::hidden('return_view','ajax-tr-task-file') !!}
										{{ csrf_field() }}



									<div class="fallback">
										<input name="file" type="file" multiple/>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>

	<div class="white-box">

		<h2>@lang('app.menu.files')</h2>

		<div class="table-responsive">
			<table class="table table-bordered table-hover toggle-circle default footable-loaded footable"
			id="files-table">
			<thead>
				<tr>
					<tr>
						<th>File</th>
						<th>Versions</th>
						<th>Creation date</th>
						<th>Last update date</th>
						<th>Actions</th>
					</tr>
				</tr>
			</thead>
			<tbody id="files-list">
				@foreach($files as $file)
				<tr>
					<td>{{$file->filename}}</td>
					<td>{{$file->fileRoot()}}</td>
					<td>{{$file->created_at}}</td>
					<td>{{$file->updated_at}}</td>
					<td>
						<a target="_blank" href="{{ asset('user-uploads/project-files/'.$file->project_id.'/'.$file->filename) }}" data-toggle="tooltip" data-original-title="View" class="btn btn-info btn-circle">
				          <i class="fa fa-eye"></i>
				        </a>
				        <a href="{{route('admin.files.download', $file->id)}}" class="btn btn-info btn-circle" data-toggle="tooltip" data-original-title="Download">
				          <i class="fa fa-download" aria-hidden="true"></i>
				        </a>
				        <a href="{{route('admin.task-file-details',['fileId'=>$file->id])}}" class="btn btn-info btn-circle" data-toggle="tooltip" data-original-title="File details">
				          <i class="fa fa-search" aria-hidden="true"></i>
				        </a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>




	</div>

</div>
</div>

</div>

@endsection
@push('footer-script')
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>

<script>
    $('#show-dropzone').click(function () {
        $('#file-dropzone').toggleClass('hide show');
    });

    $("body").tooltip({
        selector: '[data-toggle="tooltip"]'
    });

    // "myAwesomeDropzone" is the camelized version of the HTML element's ID
    Dropzone.options.fileUploadDropzone = {
        paramName: "file", // The name that will be used to transfer the file
//        maxFilesize: 2, // MB,
        dictDefaultMessage: "@lang('modules.projects.dropFile')",
        accept: function (file, done) {
            done();
        },
        init: function () {
            this.on("success", function (file, response) {
                console.log(response);
                $('#files-table tbody').html(response.html);
            })
        }
    };
</script>
@endpush
