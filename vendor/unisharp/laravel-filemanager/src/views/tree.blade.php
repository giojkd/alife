
<ul class="list-unstyled">
  @foreach($root_folders as $root_folder)
  @if($root_folder->name != 'laravel-filemanager::lfm.title-admin')
    <li>
      <a class="clickable folder-item" data-id="{{ $root_folder->path }}">
        <i class="fa fa-folder"></i> {{ $root_folder->name }}
      </a>
    </li>
    @endif
    @foreach($root_folder->children as $directory)
    <?php # if(is_numeric(trim($directory->path,'/') )){?>
      <li style="margin-left: 10px;">
        <a class="clickable folder-item" data-id="{{ $directory->path }}">
          <i class="fa fa-folder"></i> {{ $directory->name }}
        </a>
      </li>
    <?php # }?>
    @endforeach
    @if($root_folder->has_next)
      <hr>
    @endif
  @endforeach
</ul>
<?php

#echo '<pre>';
#print_r($root_folders);

?>
