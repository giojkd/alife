@forelse($files as $file)

    <tr>
      <td>{{$file->filename}}</td>
      <td>{{$file->fileRoot()}}</td>
      <td>{{$file->created_at}}</td>
      <td>{{$file->updated_at}}</td>
      <td>
          <a target="_blank" href="{{ asset('user-uploads/project-files/'.$file->project_id.'/'.$file->filename) }}" data-toggle="tooltip" data-original-title="View" class="btn btn-info btn-circle">
            <i class="fa fa-eye"></i>
          </a>
          <a href="{{route('admin.files.download', $file->id)}}" class="btn btn-info btn-circle" data-toggle="tooltip" data-original-title="Download">
            <i class="fa fa-download" aria-hidden="true"></i>
          </a>
          <a href="{{route('admin.task-file-details',['fileId'=>$file->id])}}" class="btn btn-info btn-circle" data-toggle="tooltip" data-original-title="File details">
            <i class="fa fa-search" aria-hidden="true"></i>
          </a>
      </td>
    </tr>
@empty
    <tr>
      <td>@lang('messages.noFileUploaded')</td>
    </tr>
@endforelse