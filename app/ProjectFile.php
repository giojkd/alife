<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProjectFile extends Model
{
    //

    public function fileRoot(){

    	return DB::table('project_files')->where('file_id',$this->file_id)->count();
    }

    public function versionsCount(){
    	return $this->hasMany('App\ProjectFile','file_id','file_id')->orderBy('updated_at','desc');
    }
}
