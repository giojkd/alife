<?php

namespace App\Http\Controllers\Client;

use App\EmailNotificationSetting;
use App\Helper\Reply;
use App\Http\Requests\SmtpSetting\UpdateSmtpSetting;
use App\Notifications\TestEmail;
use App\SmtpSetting;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Filemanager extends ClientBaseController
{

    public function index() {

        $this->data['pageIcon'] = '';
        $this->data['pageTitle'] = '';
        return view('client.filemanager.index', $this->data);
    }




}
