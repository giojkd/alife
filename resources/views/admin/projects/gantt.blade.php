@extends('layouts.app')

@section('page-title')
<div class="row bg-title">
  <!-- .page title -->
  <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
    <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }}</h4>
  </div>
  <!-- /.page title -->
  <!-- .breadcrumb -->
  <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
    <ol class="breadcrumb">
      <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
      <li><a href="{{ route('admin.projects.index') }}">{{ $pageTitle }}</a></li>
      <li class="active">@lang('app.addNew')</li>
    </ol>
  </div>
  <!-- /.breadcrumb -->
</div>
@endsection

@push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css') }}">
<link href="//cdn.dhtmlx.com/gantt/edge/skins/dhtmlxgantt_broadway.css" rel="stylesheet">


@endpush

@section('content')

<form class="form form-inline" action="" method="get">
  <div class="row">
    <div class="form-group col-md-1">
      <label for="">Step</label>
      <select class="form-control" name="step" placeholder="">
        @foreach ($stepOptions as $stepOption)
        <option @if($step == $stepOption) selected @endif value="{{$stepOption}}">{{$stepOption}} giorni</option>
        @endforeach
      </select>
    </div>
    <div class="col-md-1">
      <button style="margin-top: 27px;" type="submit" class="btn btn-success">Refresh</button>
    </div>

    <div class="col-md-10 text-right">
      <a style="margin-top: 27px;" href="javscript:" class="btn btn-default" onclick="exportGanttXls()">Export Gantt</a>
    </div>
  </div>
</form>


<div class="row">
  <div class="col-md-12">
    <h2>@lang('modules.projects.viewGanttChart')</h2>
    <div id="gantt_here" style='width:100%; height:100vh;'></div>
  </div>
</div>    <!-- .row -->

@endsection

@push('footer-script')
<script src="//cdn.dhtmlx.com/gantt/edge/dhtmlxgantt.js"></script>
<script src="//cdn.dhtmlx.com/gantt/edge/locale/locale_{{ $global->locale }}.js"></script>
<script src="http://export.dhtmlx.com/gantt/api.js"></script>

<script type="text/javascript">

var step = {{$step}};

gantt.config.xml_date = "%Y-%m-%d %H:%i:%s";

gantt.templates.task_class = function (st, end, item) {
  return item.$level == 0 ? "gantt_project" : ""
};

gantt.config.scale_unit = "quarter";
gantt.config.date_scale = "%F, %Y";

gantt.config.scale_height = 50;

gantt.config.subscales = [
  {unit: "day", step: step, date: "%j, %D"}
];

gantt.config.columns=[
    {name:"text",       label:"Task name",  tree:true, width:'*' },
    {name:"start_date", label:"Start time", align: "center" },
    {name:"end_date", label:"End time", align: "center" }
];

//defines the text inside the tak bars
gantt.templates.task_text = function (start, end, task) {
  if ( task.$level > 0 ){
    return task.text + ", <b> @lang('modules.tasks.assignTo'):</b> " + task.users;
  }
  return task.text;

};

gantt.attachEvent("onTaskCreated", function(task){
  //any custom logic here
  return false;
});

gantt.attachEvent("onBeforeLightbox", function(id) {
  var task = gantt.getTask(id);
  if ( task.$level > 0 ){
    $(".right-sidebar").slideDown(50).addClass("shw-rside");

    var taskId = task.id;
    var url = "{{ route('admin.all-tasks.show',':id') }}";
    url = url.replace(':id', taskId);

    $.easyAjax({
      type: 'GET',
      url: url,
      success: function (response) {
        if (response.status == "success") {
          $('#right-sidebar-content').html(response.view);
        }
      }
    });
  }
  return false;
});

gantt.attachEvent("onBeforeTaskDrag", function(id, mode, e){
  //any custom logic here
  return false;
});

gantt.init("gantt_here");

gantt.load('{{ route("admin.projects.ganttData") }}');

function exportGanttXls(){
  gantt.exportToExcel({
    name:"document.xlsx",
    columns:[
      { id:"text",  header:"Title", width:150 },
      { id:"start_date",  header:"Start date", width:250, type:"date" }
    ],
    visual:true,
    cellColors:true
  });
}

</script>
@endpush
