<?php

namespace App\Http\Controllers\Client;

use App\Helper\Reply;
use App\ModuleSetting;
use App\Project;
use App\Task;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Tasks\StoreTask;
use App\Notifications\NewTask;
use App\Notifications\TaskCompleted;
use App\Traits\ProjectProgress;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Notification;

class ClientTasksController extends ClientBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = __('app.menu.projects');
        $this->pageIcon = 'icon-layers';

        if(!ModuleSetting::checkModule('tasks')){
            abort(403);
        }
    }

    public function logSearchEntry($searchableId, $title, $route) {
      /*
        $search = new UniversalSearch();
        $search->searchable_id = $searchableId;
        $search->title = $title;
        $search->route_name = $route;
        $search->save();
      */
    }

    public function calculateProjectProgress($projectId){
        if(is_null($projectId)){
            return;
        }

        $totalTasks = Task::where('project_id', $projectId)->count();

        if($totalTasks == 0){
            return "0";
        }

        $completedTasks = Task::where('project_id', $projectId)
            ->where('status', 'completed')
            ->count();
        $percentComplete = ($completedTasks/$totalTasks)*100;

        $project = Project::findOrFail($projectId);
        if($project->calculate_task_progress == "true"){
            $project->completion_percent = $percentComplete;
        }
        $project->save();

        return $percentComplete;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(StoreTask $request)
     {
         $task = new Task();
         $task->heading = $request->heading;
         if($request->description != ''){
             $task->description = $request->description;
         }
         $task->due_date = Carbon::parse($request->due_date)->format('Y-m-d');
         $task->user_id = $request->user_id;
         $task->project_id = $request->project_id;
         $task->priority = $request->priority;
         $task->status = 'incomplete';
         $task->save();

 //      Send notification to user
         $notifyUser = User::findOrFail($request->user_id);
         $notifyUser->notify(new NewTask($task));

         $this->logProjectActivity($request->project_id, __('messages.newTaskAddedToTheProject'));

         $this->project = Project::findOrFail($task->project_id);
         $view = view('admin.projects.tasks.task-list-ajax', $this->data)->render();

         //calculate project progress if enabled
         $this->calculateProjectProgress($request->project_id);

         //log search
         $this->logSearchEntry($task->id, 'Task: '.$task->heading, 'admin.all-tasks.edit');

         return Reply::successWithData(__('messages.taskCreatedSuccessfully'), ['html' => $view]);
     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->task = Task::findOrFail($id);
        $view = view('client.tasks.show', $this->data)->render();
        return Reply::dataOnly(['status' => 'success', 'view' => $view]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->tasks = Task::leftJoin('projects', 'projects.id', '=', 'tasks.project_id')
            ->where('tasks.project_id', '=', $id)
            ->where('projects.client_id', '=', $this->user->id)
            ->select('tasks.*')
            ->get();
        $this->project = Project::findOrFail($id);

        if($this->project->client_view_task == 'disable'){
            abort(403);
        }

        return view('client.tasks.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
